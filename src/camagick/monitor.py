#!/usr/bin/python3

from matplotlib import pyplot as plt
from matplotlib import transforms
from epics import caget

import numpy as np

import os, sys, time, logging, math


if __name__ == "__main__":

    prefix = sys.argv[1] if len(sys.argv)>1 else "KMC3:XPP:GONIOCAM:"

    bgcolor='#404050'
    fgcolor='w'
    
    fig = plt.figure(facecolor=bgcolor)
    grid = fig.add_gridspec(2, 2, width_ratios=[4,1], height_ratios=[4,1])
    ax_pic = fig.add_subplot(grid[0,0], facecolor=bgcolor)
    ax_xsum = fig.add_subplot(grid[0,1], facecolor=bgcolor)
    ax_ysum = fig.add_subplot(grid[1,0], facecolor=bgcolor)
    ax_xsum.set(xticks=[])
    ax_xsum.set_mouseover(True)
    ax_ysum.set(yticks=[])
    ax_ysum.set_mouseover(True)
    grid.tight_layout(fig)
    
    obj_pic = ax_pic.imshow( (np.array([[0, 1], [1, 0]])*256).astype(np.uint64), aspect='auto')
    obj_xsum = ax_xsum.plot([])[0]
    obj_ysum = ax_ysum.plot([])[0]

    obj_xstat = ax_xsum.plot([], marker='+', linestyle='solid')[0]
    obj_ystat = ax_ysum.plot([], marker='+', linestyle='solid')[0]

    fig.show()
                
    frm_shape = caget(prefix+"frameShape").astype(int)
    print("Shape:", frm_shape)
    
    obj_pic.set_extent((0, frm_shape[1], 0, frm_shape[0]))

    axis_x = np.array(range(frm_shape[0]))
    axis_y = np.array(range(frm_shape[1]))
   
    ax_xsum.set_ylim(0, frm_shape[0])
    ax_ysum.set_xlim(0, frm_shape[1])

    ax_xsum.set_xlim(0, 1)
    ax_ysum.set_ylim(0, 1)
            
    
    while True:
        try:
            frm_flat = np.array(caget(prefix+"flatFrame"), dtype=int).reshape(frm_shape)
            
            frm_x = np.array(caget(prefix+"frameSumX"), dtype=int)
            frm_y = np.array(caget(prefix+"frameSumY"), dtype=int)
            
            frm_com = caget(prefix+"centerOfMass")
            frm_std = caget(prefix+"standardWidth")
            
            frame = np.array(frm_flat)
            
            obj_pic.set_data(frame[::-1,:])
            
            obj_xsum.set_data(frm_x/frm_x.max(), axis_x)
            obj_ysum.set_data(axis_y, frm_y/frm_y.max())

            
            stdf = math.sqrt(2*math.log(2))
            
            obj_ystat.set_data([frm_com[1]-stdf*frm_std[1],
                                frm_com[1],
                                frm_com[1]+stdf*frm_std[1]],
                               [0.5, 0.5, 0.5])
            
            obj_xstat.set_data([0.5, 0.5, 0.5],
                               [frm_com[0]-stdf*frm_std[0],
                                frm_com[0],
                                frm_com[0]+stdf*frm_std[0]])
            
        except Exception as e:
            logging.error(e)
            #raise
    
        fig.canvas.draw_idle()
        fig.canvas.flush_events()
        time.sleep(0.01)
