# What is CAMagick?

CAMagick is a set of tools aimed at client-side interaction with
data from the [EPICS](https://epics-controls.org/) Channel-Access protocol.
When it grows up, it wants to be to EPICS what ImageMagick is to graphics ;-)

# Quick Inspiration

The control of camagick tools takes place mostly from the Linux command
line. 

The general idea is that somewhere, somehow, some device exports one or
several streams of data using the EPICS CA protocol, and you (the engineer
or physicist) needs to rapidly view that data. Optionally, you can do
some light processing on it before displaying it.

## Data model

### Types of data

In principle there are several types of data:

  - **numerical scalars**, i.e. a temperature or a current, integer or
    floating point, binary 0/1 information etc.
  
  - **discrete tokens**, e.g. a string or a binary information resolved into a
    string. The demarcation to "numerical scalars" is not all that clear here, 
	for instance a numerical state variable could be viewed as a token, or
	as a numerical scalar. Quite generallly, if they can be "higher" or "lower",
	they're scalars; if not, they're tokens.
  
  - **waveforms**, e.g. a signal along a specified axis. Typically EPICS/CA
    only have 1-dimensional waveforms, but we like to work with multidimensional
	data (e.g. images of a detector).

### Data collection  / current context

There are several scenarii of viewing that we can emply:

  - **gauge**, i.e. see the current state or value of a process variable (PV)
  
  - **track**, i.e. follow the evolution of a process variable over time.
    Essentially "tracking" means adding a supplementary dimension (time)
	to the data in a revolving manner, and displaying the result.
  

## Processing model

### Processing actors

The entities of data that can be involved in operations are
(need better names for this):

  - the "cursor data", i.e. the piece of data that's naturally currently in
    the focus, is is defined by the current value of the PV in question (or
    set of PVs)
	
  - the "past data", i.e. what is already there: in tracking mode it's
    the track collection, in gauging mode it's the previous value
 
  - the "result data", i.e. the current processing step's output

### Operations on data

Operations to be peformed on a single quantity (current cursor):

  - **integration**, i.e. reduction in dimensionality by integrating over points
  
  - **reshaping**, i.e. changing dimensionality by modifying the data layout,
    but not changing the data
  
  - **binning**, i.e. reducing dimension (but not dimensionality)
  
  - **filtering**, i.e. keeping data format, but modifying the data
    points (e.g. by smoothing, fitting).


Operations to be performed on multiple signals:

  - **concatenating**, i.e. appending a signal to another (e.g. along the same,
    or along a new dimension)
	
  - **arithmetics**, i.e. adding/substracting/multiplying signals

## I/O Model

### Acquisition mechanisms

New data frames can be acquired:

  - **triggered**, i.e. on value change of a "trigger" PV, either to 
    a specific value, or to any new value
  
  - **polled**, i.e. by periodic query by the client application
    in regular intervals.


### Input/Output channels


Data sources:

  - EPICS PVs, i.e. named variables ("cursor" and "past" data)
  
  - internal operations ("result data")
  
  - storage / pipes from disk or stdin ("cursor" and "past" data)


Data sinks:

  - **write**: EPICS PVs (i.e. other named variables)
  
  - **publish**: IOCs mode (i.e. *this* application starts exporting new PVs)

  - **view**: visualisation (e.g. matplotlib pannels)
  
  - **save**: storage / pipes (write to disk or stdout)


## Usage examples

A.k.a. "CLI user stories" :-)

  - Display a single value (or waveform) in its default form:
    ```
    camagick gauge PREFIX:VALUE view
    ```
	
  - Display time evolution of a value, or a waterfall signal diagram
    of a waveform:
    ```
	camagick track PREFIX:VALUE view
	```
	
  - Take a camera image, reshape to with/height, display data integrated along the
    X or Y axis:
    ```
	camagick gauge PREFIX:IMAGE reshape widthxheight integrate 0 view
	camagick gauge PREFIX:IMAGE reshape widthxheight integrate -axis 1 view
	```
	
	FIXME: how to split the processing pipeline in order to integrate along
    both X *and* Y from the same image? Possibly make integrating over each
	dimension in turn an explicit operation, and call that as an operation
	```
	camagick gauge PREFIX:IMAGE reshape widthxheight integrate -marginals view
	```
	
	FIXME: how does the `-marginals` and the follwing `view` know how to
	display the data? I.e. the main image in the center, and each of the
	integrated slices along the corresponding sides? (Here the image is
	"cursor", and marginals are "result"... or not?)
	
	
  - Take camera image, rehsape, integrate, pass data for further processing
    (fitting) to external application via stdout, then display:
	```
	camagick gauge PREFIX:IMAGE reshape widthxheight integrate -axis 0 save - | \
	   external_fit | \
	   camagick gauge - view
	```
	
	FIXME: looks difficult to use. Better to integrate basic fitting into
	the camagick command line, e.g. `integrate -axis 0 fit 'gauss ...' view`.
	
	FIXME: how to display raw data *and* fitted data in the same plot?
	
	FIXME: how to display them in separate plots?
